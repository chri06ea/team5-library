﻿using System;

namespace MyMath
{
    static public class MMath
    {
        public static double Add(double v1, double v2)
        {
            return v1 + v2;
        }

        public static double Subtract(double v1, double v2)
        {
            return v1 - v2;
        }

        public static double Multiply(double v1, double v2)
        {
            return v1 * v2;
        }

        public static double Divide(double v1, double v2)
        {
            return v1 / v2;
        }

        public static double SquareRoot(double v)
        {
            return System.Math.Sqrt(v);
        }

        public static double Pow(double v, double power)
        {
            return Math.Pow(v, power);
        }

        public static double Percentage(double v, double min, double max)
        {
            return (v - min) / (max - min);
        }

        public class Circle
        {
            double radius;
        }

        public class Square
        {
            double length, width;
        }

        public class Vector
        {
            public Vector()
            {
                x = 0; y = 0; z = 0;
            }

            public Vector(Vector v)
            {
                this.x = v.x;
                this.y = v.y;
                this.z = v.z;
            }

            public Vector(double x, double y, double z)
            {
                this.x = x;
                this.y = y;
                this.z = z;
            }

            static public Vector operator +(Vector v1, Vector v2)
            {
                return new Vector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
            }

            static public bool operator ==(Vector v1, Vector v2)
            {
                return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z;
            }
            
            static public bool operator !=(Vector v1, Vector v2)
            {
                return v1.x != v2.x || v1.y != v2.y || v1.z != v2.z;
            }

            public double Length()
            {
                return SquareRoot(x * x + y * y + z * z);
            }

            public void Scale(double v)
            {
                this.x *= v;
                this.y *= v;
                this.z *= v;
            }

            public static Vector Add(Vector v1, Vector v2)
            {
                return new Vector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
            }

            public static Vector Subtract(Vector v1, Vector v2)
            {
                return new Vector(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
            }

            public static Vector Multiply(Vector v1, Vector v2)
            {
                Vector v = new Vector();
                v.x = v1.x * v2.x;
                v.y = v1.y * v2.y;
                v.z = v1.z * v2.z;
                return v;
            }

            public double x, y, z;
        }
    }
}
