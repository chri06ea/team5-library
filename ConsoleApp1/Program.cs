﻿using System;
using MyMath;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Indtast nummer 1");

            var strNummer1 = Console.ReadLine();

            var nummer1 = double.Parse(strNummer1);

            Console.WriteLine("Indtast nummer 2");

            var strNummer2 = Console.ReadLine();

            var nummer2 = double.Parse(strNummer2);

            Console.WriteLine($"{strNummer1} + {strNummer2} = {MMath.Add(nummer1, nummer2)}");

            Console.WriteLine($"{strNummer1} - {strNummer2} = {MMath.Subtract(nummer1, nummer2)}");
            
            Console.WriteLine($"{strNummer1} * {strNummer2} = {MMath.Multiply(nummer1, nummer2)}");

            Console.WriteLine($"{strNummer1} / {strNummer2} = {MMath.Divide(nummer1, nummer2)}");

            Console.WriteLine($"Kvadratrod af {strNummer1} =  {MMath.SquareRoot(nummer1)}");

            Console.WriteLine($"Kvadratrod af {strNummer2} =  {MMath.SquareRoot(nummer2)}");

            Console.ReadLine();

        }
    }
}
