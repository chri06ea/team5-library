using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyMath;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(2, MMath.Add(1, 1));
        }

        [TestMethod]
        public void TestMethod2()
        {
            Assert.AreEqual(3, MMath.Subtract(4, 1));
        }

        [TestMethod]
        public void TestMethod3()
        {
            var v1 = new MMath.Vector(5, 5, 5);
            Assert.AreEqual(8.660254037844387, v1.Length());
        }

        [TestMethod]
        public void TestMethod4()
        {
            Assert.IsTrue(new MMath.Vector(1,2,3) != new MMath.Vector(3,2,1));
        }

        [TestMethod]
        public void TestMethod5()
        {
            var v = new MMath.Vector(1, 1, 1) + new MMath.Vector(2, 2, 2);

            Assert.IsTrue(v == new MMath.Vector(3,3,3));
        }
    }
}
